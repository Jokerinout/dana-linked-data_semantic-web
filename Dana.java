import java.io.InputStream;

import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.FileManager;

public class TP1 {
	static final String file  = "rdfPrelevement.ttl";

	public static void main (String args[]) {
		Model model = ModelFactory.createDefaultModel();

		InputStream in = FileManager.get().open( file );
		if (in == null) {
			throw new IllegalArgumentException( "File: " + file + " not found");
		}

		model.read(in, null, "Turtle");

		String prefix = "PREFIX dcterms: <http://purl.org/dc/terms/>\n" + 
				"PREFIX org: <http://www.w3.org/ns/org#>\n" + 
				"PREFIX npg: <http://ns.nature.com/terms/>\n" + 
				"PREFIX sp: <http://spinrdf.org/sp#>\n" + 
				"PREFIX seas-eval: <https://w3id.org/seas/>\n" + 
				"PREFIX juso.kr: <http://rdfs.co/juso/kr/>\n" +
				"PREFIX isoadr: <http://reference.data.gov.au/def/ont/iso19160-1-address#>\n" +
				"PREFIX osp: <http://data.lirmm.fr/ontologies/osp#>\n" +
				"PREFIX og: <http://ogp.me/ns#>\n" +
				"PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>\n" +
				"PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>\n" +
				"PREFIX ex: <http://ex.org/a#>\n" +
				"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n";

		String queryString1 = prefix + "select ?orga ?muni ?codePostal ?ape ?date ?valeur\n"
				+ "where {\n"
				+ "?x sp:Sample ?y;\n"
				+ "   npg:date ?date;\n"
				+ "   seas-eval:value ?valeur;\n"
				+ "   dbpedia-owl:Company ?org.\n"
				+ "?org org:organization ?orga;\n"
				+ "     dbpedia-owl:municipality ?muni;\n"
				+ "     isoadr:adress.business_branch ?ape;\n"
				+ "     og:postal-code ?codePostal.\n"
				+ "filter(?y='Prelevements Eaux Souterraines')\n"
				+ "filter(?date > 2003)\n"
				+ "}"
				+ "order by ?date \n"
				+ "limit 10 \n";

		String queryString2 = prefix + "select ?orga ?ape ?date ?valeur\n"
				+ "where {\n"
				+ "?x npg:date ?date;\n"
				+ "   seas-eval:value ?valeur;\n"
				+ "   dbpedia-owl:Company ?org.\n"
				+ "?org org:organization ?orga;\n"
				+ "     dbpedia-owl:municipality ?y;\n"
				+ "     isoadr:adress.business_branch ?ape;\n"
				+ "filter(?y='NANTES')\n"
				+ "filter(?date > 2003)\n"
				+ "}"
				+ "order by ?date \n"
				+ "limit 10 \n";


		Query query = QueryFactory.create(queryString2) ;
		try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
			ResultSet results = qexec.execSelect() ;
			ResultSetFormatter.out(System.out, results, query) ;
		}
	}
}
