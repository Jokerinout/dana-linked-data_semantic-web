Web des données, Web sémantique.

Présentation du projet Dana 2019:
	
	-Database: Registres français des émissions polluantes - Prélèvement
	 Link: https://public.opendatasoft.com/explore/dataset/registre-francais-des-emission-polluantes-prelevements
	
	-Conversion CSV -> triplets RDF : Tarql
	 Link: https://github.com/tarql/tarql

	-Pollution.ttl est le dataset de Clément, avec lequel nous nous sommes liés notamment (voir diapo numéro 2)
	
	-Le fichier owl est le fichier protégé pour la partie 3
	
	-Pour les autres questions: voir diapo.
